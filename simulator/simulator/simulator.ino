

#include <SPI.h>
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>




  
int led = 2; 
int intervalo = 5000;
int now = millis();
int button = 4; //D2(gpio4)
int ledpin = 5; // D1(gpio5)
int buttonState=0;
int dartNumber = 1;
int player = 1;
 

void setup() {

  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);
  pinMode(button, INPUT);
  pinMode(ledpin, OUTPUT);
  
  Serial.begin(9600);
  while (!Serial)
    ;
}

void loop() {
  read_button();
  if (millis() - now >= intervalo) {
    String json = create_and_send_json();
    Serial.println(json);
    now = millis();
  }
  
}


String create_and_send_json(){
  int randNumber = random(1, 20);
  int randMultiplicator = random(1,4);
  int value = randMultiplicator * randNumber;
  int winner = random(0,10);
  dartNumber += 1;
  if (dartNumber >= 4){
    dartNumber = 1;
    player += 1;
    if (player >= 5)
      player = 1;
  }
  return "{'player':"+String(player)+",'new_value':"+String(value)+", 'multiplicator': " + String(randMultiplicator) + ", 'original_value':"+String(randNumber)+", 'winner':"+String(winner)+"}";
}

void read_button(){
  
  buttonState=digitalRead(button); // put your main code here, to run repeatedly:
  if (buttonState == 1)
  {
   digitalWrite(ledpin, HIGH); 
   delay(200);
  }
  if (buttonState==0)
  {
   digitalWrite(ledpin, LOW); 
   delay(200);
  }
}
