# mysite/routing.py
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from settings.routing import websocket_urlpatterns
# from channels.staticfiles import StaticFilesConsumer

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    # 'http.request': StaticFilesConsumer(),
    'websocket': AuthMiddlewareStack(
        URLRouter(
            websocket_urlpatterns
        )
    ),
})
