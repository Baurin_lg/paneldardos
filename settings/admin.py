from django.contrib import admin
from django.contrib.auth.models import User, Group

# Register your models here.

from .models import *

admin.site.site_header = "Dardos Mikel Admin"
admin.site.site_title = "Dardos Mikel Admin"
admin.site.index_title = "Welcome to Dardos Panel Management for administrators"

admin.site.register(Settings)
admin.site.register(Trace)