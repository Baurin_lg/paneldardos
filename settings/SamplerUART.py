import time
import threading
import glob
import serial
from .models import Trace
import random
import json

class SamplerUART():

    def __init__(self):

        try:
            self.serial = serial.Serial(port=self.serial_ports()[0], baudrate=9600)
        except Exception as e:
            print ("[ FAIL ] - No simulator connected")

        thread = threading.Thread(target=self.__polling_serial, args=())
        thread.start()

    def serial_ports(self):

        ports = glob.glob('/dev/tty[A-Za-z]*')

        result = []
        for port in ports:
            try:
                ser = serial.Serial(port)
                ser.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        if len(result) == 0:
            result = ["/dev/ttyUSB0"]

        return result

    def __polling_serial(self):

        # player = 1
        input_json = b''
        extracting = False
        while True:
            char_in = self.serial.read(1)
            if char_in == b'{':
                extracting = True
            if extracting:
                input_json += char_in
            if char_in == b'}':
                extracting = False
                print (input_json)
                new_trace = Trace.objects.create(
                    trace_json=input_json.decode("utf8").replace("'", "\"")
                )
                print ("-----------------------------")
                input_json = b''

        # while True:

        #     if player >= 5: player = 1

        #     for iterator in range(1,4):
        #         multiplicator = random.randint(1,4)
        #         new_value = random.randint(1,20)
        #         new_trace = {
        #             "player": player,
        #             "new_value": new_value * multiplicator,
        #             "multiplicator": 1,
        #             "dart_number": iterator,
        #             "original_value": new_value
        #             "winner": "0"
        #         }
        #         new_trace = Trace.objects.create(
        #             trace_json=json.dumps(new_trace)
        #         )
        #         time.sleep(2)

        #     player += 1

        #     print (new_trace.trace_json)
