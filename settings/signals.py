
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from .models import Trace, Settings

import sys
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
import time
from django.conf import settings



@receiver(post_save, sender=Trace)
def new_mqtt_published(sender, instance, created, **kwargs):

    if created:
        layer = get_channel_layer()

        print ("Sending to Front Web")

        try:
            async_to_sync(layer.group_send)('dardos_address', {
                'type': 'new_trace',
                'message': 'new_trace',
                'data': {
                    'data': 'Here is the data',
                    'trace_json': instance.trace_json,
                }
            })
        except Exception as e:
            print (str(e))