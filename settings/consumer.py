from channels.generic.websocket import AsyncWebsocketConsumer
import json
from asgiref.sync import async_to_sync
from .models import Trace
from channels.db import database_sync_to_async


class TraceConsumer(AsyncWebsocketConsumer):

    groups = ["broadcast"]

    async def connect(self):


        print ("New User Connected")
        self.dardos_id = 1
        self.dardos_group_name = 'dardos_address'

        await self.channel_layer.group_add(self.dardos_group_name, self.channel_name)
        await self.channel_layer.group_add('events', self.channel_name)
        await self.accept()


    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.dardos_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):

        text_data_json = json.loads(text_data.replace("'", "\""))
        print (text_data_json)
        self.var_dardos = await self.get_var_dardos(text_data_json) 

    # Receive message from room group
    async def new_trace(self, event):
        print (event['message'])
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'type': 'dardos-update',
            'data': event['data']
        }))

    @database_sync_to_async
    def get_var_dardos(self, text_data_json):

        return Trace.objects.all()
