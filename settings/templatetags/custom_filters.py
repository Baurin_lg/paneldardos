from django import template

register = template.Library()

@register.filter
def in_decadd(things, decAdd):
    return things.filter(decAdd=decAdd)


@register.filter
def convert_byte_to_string(value):
    return value.decode("utf-8") if value else value