from django.db import models

# Create your models here.


class Settings(models.Model):

    company = models.CharField(blank=True, null=True, max_length=100)
    logo = models.CharField(blank=True, null=True, max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):

        return "%s - <RF: %s> - Created: %s " % (self.ip, self.working_rf, self.created)


class Trace(models.Model):

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    trace = models.BinaryField(null=True, blank=True)
    trace_json = models.TextField(blank = False)

