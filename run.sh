check_process=`ps aux| grep runserver| grep -v grep`

if [ "$check_process" == "" ]; then
	echo "Lanzando Servidor"
	cd /mnt/sda1/tce/app
	source env/bin/activate
	cd modemsys_tsm
	python3 manage.py runserver 0:8000 >> ../log_server.log 2>&1 &
	celery -A tsm.celery worker >> /tmp/celery_app.log 2>&1 &
	echo "Servidor activo"
else
	echo "El proceso ya esta lanzado"
fi
